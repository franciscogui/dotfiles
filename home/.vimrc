runtime defaults.vim
 
set number
set relativenumber
set laststatus=2
set hidden
set ignorecase
set autoindent
set expandtab
set cursorline
set mouse=a
set tabstop=4
set clipboard=unnamedplus

call plug#begin()

Plug 'dracula/vim', { 'as': 'dracula' }
"comment when install new env: run PlugInstall the fisrt a pluggin is added

call plug#end()

color dracula
