## qTile configuration file
## Two setups are possible:
## - One monitor with two bars (top/bottom)
## - Three monitors, main monitor with top/bottom bars and secondary monitors with bottom bar  

import os
import re
import socket
import subprocess
import json
import requests
from libqtile.config import Drag, Match, Key, KeyChord, Screen, Group, ScratchPad, DropDown, Drag, Click, Rule
from libqtile.lazy import lazy
from libqtile import qtile, layout, bar, widget, hook, extension
import arcobattery

#############################################################################
############################ KEY BINDINGS ###################################
#############################################################################

mod = "mod4" #mod4 or mod = super key 
mod1 = "alt"
mod2 = "control"

keys = [

    Key([mod], "f", lazy.window.toggle_fullscreen(), desc= "Toggles fullscreen"),
    Key([mod], "a", lazy.window.toggle_fullscreen(), desc= "Toggles max/matrix layout"),
    Key([mod], "q", lazy.window.kill(), desc= "Kills the current window"),
    Key([mod, mod2], "r", lazy.restart(), desc= "Restarts qtile"),
    Key([mod], "space", lazy.next_layout(), desc= "Switches to next layout"),
    Key([mod], "n", lazy.next_screen(), desc= "Sets next screen as active"),

    Key([mod], "Tab", lazy.screen.next_group(), desc= "Selects next group"),
    Key([mod, "shift" ], "Tab", lazy.screen.prev_group(), desc= "Selects previous group"),
    Key(["mod1"], "Tab", lazy.screen.next_group()),
    Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),
    
# window focus
    Key([mod], "k", lazy.layout.up(), desc= "Sets focus to the window above"),
    Key([mod], "j", lazy.layout.down(), desc= "Sets docus to the window below"),
    Key([mod], "h", lazy.layout.left(), desc= "Sets focus to the window on the left"),
    Key([mod], "l", lazy.layout.right(), desc= "Sets focus to the window on the right"),

# window resize
    Key([mod, mod2], "k", lazy.layout.grow_up(), lazy.layout.grow(), lazy.layout.decrease_nmaster(), desc= "Resizes window growing up"),
    Key([mod, mod2], "j", lazy.layout.grow_down(), lazy.layout.shrink(), lazy.layout.increase_nmaster(), desc= "Resizes window growing down"),

# move window  (monad layouts)
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc= "Moves window up (monad layouts only)"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(), desc= "Moves window  down (monad layouts only)"),
    Key([mod, "shift"], "Left", lazy.layout.swap_left(), desc= "Moves window left (monad layouts only)"),
    Key([mod, "shift"], "Right", lazy.layout.swap_right(), desc= "Moves window right (monad layouts only)"),
]

#############################################################################
############################ GROUP DEFINITIONS ##############################
#############################################################################

groups = []
group_names = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
group_labels = ["", "", "", "", "", "", "", "", "", ""]
group_layouts = ["monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "monadtall", "treetab", "treetab"]

for i in range(len(group_names)):
    groups.append(
        Group(
            name=group_names[i],
            layout=group_layouts[i].lower(),
            label=group_labels[i],
        ))


for i in groups:
    keys.extend([
        Key([mod], i.name, lazy.group[i.name].toscreen(), desc= "Changes the active group"),
        Key([mod, "mod1"], i.name, lazy.window.togroup(i.name), desc= "Move window to group staying on the current one"),
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen(), desc= "Move and follow window to group"),
    ])

groups.append(
        ScratchPad("scratchpad", [
                DropDown("term", "alacritty --hold", y=0.18, height=0.650, on_focus_lost_hide = False),
                DropDown("emacs", "emacs", y=0.08, height=0.850, on_focus_lost_hide = False)
            ]))

keys.extend([
                Key([mod], 'm', lazy.group['scratchpad'].dropdown_toggle('term')),
                Key([mod], 'e', lazy.group['scratchpad'].dropdown_toggle('emacs'))
            ])


#############################################################################
############################ LAYOUT DEFINITIONS #############################
#############################################################################

def init_layout_theme():
    return {"margin": 8,
            "border_width": 2,
            "border_focus": "#3384d0",
            "border_normal": "#4c566a"
            }

layout_theme = init_layout_theme()

layouts = [
    layout.MonadTall(**layout_theme),
    ## layout.MonadWide(**layout_theme),
    ## layout.Floating(**layout_theme),
    layout.Max(**layout_theme),
    ## layout.Matrix(**layout_theme),
    ## layout.Stack(**layout_theme),
    layout.TreeTab(
        font = "Noto Sans Bold",
        fontsize = 10,
        sections = ["FIRST", "SECOND"],
        section_fontsize = 10,
        border_width = 2,
        bg_color = "1c1f24",
        active_bg = "fba922",
        active_fg = "000000",
        inactive_bg = "3384d0",
        inactive_fg = "000000",
        padding_left = 0,
        padding_x = 0,
        padding_y = 5,
        section_top = 10,
        section_bottom = 20,
        level_shift = 8,
        vspace = 3,
        panel_width = 150
        )
    ]

#############################################################################
############################ WIDGETS/BAR DEFINITIONS ########################
#############################################################################

def init_colors():
    return [
        ["#2F343F", "#2F343F"], # color 0
        ["#000000", "#000000"], # color 1
        ["#c0c5ce", "#c0c5ce"], # color 2
        # ["#fba922", "#fba922"], # color 3
        ["#3384d0", "#3384d0"],
        ["#3384d0", "#3384d0"], # color 4
        ["#f3f4f5", "#f3f4f5"], # color 5
        ["#cd1f3f", "#cd1f3f"], # color 6
        # ["#62FF00", "#62FF00"], # color 7
        ["#3384d0", "#3384d0"], # color 7
        ["#6790eb", "#6790eb"], # color 8
        ["#a9a9a9", "#a9a9a9"], # color 9
        ["#2EB398", "#2EB398"], # color 10
        # ["#4E9A06", "#4E9A06"], # color 11
        ["#3384d0", "#3384d0"], # color 7
        ["#fba922", "#fba922"],
    ] 

colors = init_colors()
widget_defaults = dict(font="Noto Sans Bold", fontsize = 12, padding = 2, background=colors[1])

def pr_count():
    try:
        ## place env variables in .xprofile
        resp = requests.get(url=os.getenv('DEVOPSURL'), auth=('', os.getenv('DEVOPSTOKEN')))
        return "Pull Requests (" + str(resp.json()["count"]) + ")"
    except:
        return "Pull Requests (-)"

def current_window_class():
    try:
        return str(qtile.current_window.get_wm_class()[0])
    except:
        return "(no class info)"

def init_widgets_bottom():
    return [
            widget.CurrentScreen(active_text = 'Active', inactive_text = 'Inactive', padding = 6),
            widget.WindowCount(text_format = '({num})'),
            widget.TextBox(font="Symbols Nerd Font" , fontsize = 42, background = colors[11], foreground = colors[1],  text='\ue0b0', padding = -1),
            widget.CurrentLayoutIcon(background = colors[11], scale = 0.5),
            widget.CurrentLayout(background = colors[11]),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 42, foreground = colors[11], text='\ue0b0', padding = -1),
            widget.GenPollText(font = "Noto Sans Bold Italic", foreground = colors[5], update_interval = .5, func = current_window_class , padding = 6),
            widget.Spacer(),
            widget.Pomodoro(length_pomodori = 30),
            widget.GenPollText(update_interval = 120, func = pr_count, mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('firefox --new-tab https://dev.azure.com/thula-is/_git/SFM/pullrequests')}, padding = 6),
        ]

def init_widgets_top():
    home = os.path.expanduser('~')
    return [
            widget.GroupBox(font="FontAwesome", fontsize = 16, margin_y = 2, margin_x = 0, padding_y = 6, padding_x = 5, borderwidth = 0, disable_drag = True, 
                 active = colors[3],inactive = colors[5], rounded = False, highlight_method = "text", this_current_screen_border = colors[12], spacing= 4,
                 foreground = colors[2]),
            widget.Spacer(),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, foreground = colors[11], text='\ue0b2', padding = 0),
            widget.Memory(background = colors[11], mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('alacritty -e htop')},padding = 6),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, background = colors[11], foreground = colors[1], text='\ue0b2', padding = -1),
            widget.CPU(format = "CPU: {load_percent}%  ", mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('alacritty -e htop')}),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, foreground = colors[11], text='\ue0b2', padding = 0),
            # widget.Wttr(background = colors[11], format = "%c %t", update_interval = 30, location = { "Akureyri": "Akureyri"}),
            widget.Wttr(background = colors[11], format = "%c %t/f%f %w  %d %M%m", location = { "Setúbal": "Setubal"}, update_interval = 600, padding = 6),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, background = colors[11], foreground = colors[1], text='\ue0b2', padding = -1),
            widget.Clock(foreground = colors[5], format="%d-%m-%Y %H:%M:%S  ", mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('alacritty --hold -e cal -S -w -n 6')}  ),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, foreground = colors[11], text='\ue0b2', padding = 0),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 14, background = colors[11], text="  ", mouse_callbacks={'Button1': lambda: qtile.cmd_spawn('pavucontrol')}, padding = 0),
            widget.Volume(background = colors[11], padding = 6),
            widget.TextBox(font="Symbols Nerd Font", fontsize = 58, background = colors[11], foreground = colors[1], text='\ue0b2', padding = -1),
            widget.Systray(background=colors[1], icon_size=16, padding = 6)
           # arcobattery.BatteryIcon(
           #              padding=0,
           #              scale=0.6,
           #              y_poss=3,
           #              theme_path=home + "/.config/qtile/icons/battery_icons_horiz",
           #              update_interval = 5
           #              ),
           # widget.Battery(format = '{percent:2.0%}'),
            #widget.Sep(foreground = colors[2], linewidth = 0, padding = 5),
            #widget.TextBox(font="Symbols Nerd Font", fontsize = 58, foreground = colors[11], text='\ue0b2', padding = 0)

            #widget.TextBox(font="Symbols Nerd Font", fontsize = 20, background=colors[11], text="\uf705", mouse_callbacks={'Button1': lambda: qtile.cmd_shutdown()}, padding = 6),
        ]

widgets_list = init_widgets_top()

widgets_screen1 = init_widgets_top()
widgets_screen2 = init_widgets_bottom()
widgets_screen3 = init_widgets_bottom()

def init_screens():
    # I only use either 1 monitor or three. This function controls whcich bars to load on each case
    if b'1' in subprocess.Popen('xrandr | grep " connected" | wc -l', shell=True, stdout=subprocess.PIPE).communicate()[0] :
        return [Screen(top=bar.Bar(widgets=init_widgets_top(), size=26, opacity=0.8, background = colors[1]),
                bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=0.8, background = colors[1]))]

    if b'2' in subprocess.Popen('xrandr | grep " connected" | wc -l', shell=True, stdout=subprocess.PIPE).communicate()[0] :
        return [Screen(top=bar.Bar(widgets=init_widgets_top(), size=26, opacity=1, background = colors[1]),
                   bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=1, background = colors[1])),
                Screen(bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=0.8, background = colors[1]))]
    
    return [Screen(bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=0.8, background = colors[1])),
            Screen(top=bar.Bar(widgets=init_widgets_top(), size=26, opacity=1, background = colors[1]),
                   bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=1, background = colors[1])),
            Screen(bottom=bar.Bar(widgets=init_widgets_bottom(), size=26, opacity=0.8, background = colors[1]))]

screens = init_screens()

# MOUSE CONFIGURATION
mouse = [
    Click([mod], "Button1", lazy.window.bring_to_front()),
    Click([mod], "Button3", lazy.window.disable_floating()),
    Drag([mod], "Button1", lazy.window.set_position_floating(), start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size())
]

dgroups_key_binder = None
dgroups_app_rules = [
    Rule(Match(wm_class=["Thunar"]), group="0"),
    Rule(Match(wm_class=["Brave-browser"]), group="0"),
    Rule(Match(wm_class=["code-oss"]), group="0"),
    Rule(Match(wm_class=["lens"]), group="0"),
    Rule(Match(wm_class=["slack"]), group="2"),
    Rule(Match(wm_class=["jetbrains-rider"]), group="4"),
    Rule(Match(wm_class=["azuredatastudio"]), group="5"),
    Rule(Match(wm_class=["virtualbox"]), group="7"),
    Rule(Match(wm_class=["Microsoft Teams - Preview"]), group="9"),
    Rule(Match(wm_class=["spotify"]), group="9")
]

main = None

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/scripts/layout-2-monitors.sh'])
    subprocess.call([home + '/.config/qtile/scripts/autostart.sh'])

@hook.subscribe.client_new
def set_floating(window):
    if (window.window.get_wm_transient_for()
            or window.window.get_wm_type() in floating_types):
        window.floating = True

floating_types = ["notification", "toolbar", "splash", "dialog"]

@hook.subscribe.float_change
def center_window():
    client = qtile.current_window
    if not client.floating or client.fullscreen:
        return

    screen_rect = qtile.current_screen.get_rect()

    center_x = screen_rect.x + screen_rect.width / 2
    center_y = screen_rect.y + screen_rect.height / 2

    x = center_x - client.width / 2
    y = center_y - client.height / 2
        
    # don't go off the right...
    x = min(x, screen_rect.x + screen_rect.width - client.width)
    # or left...
    x = max(x, screen_rect.x)
    # or bottom...
    y = min(y, screen_rect.y + screen_rect.height - client.height)
    # or top
    y = max(y, screen_rect.y)
    
    client.x = int(round(x))
    client.y = int(round(y))
    qtile.current_group.layout_all()

follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    *layout.Floating.default_float_rules,
    Match(wm_class='Arcolinux-welcome-app.py'),
    Match(wm_class='Arcolinux-tweak-tool.py'),
    Match(wm_class='Arcolinux-calamares-tool.py'),
    Match(wm_class='evolution-alarm-notify'),
    Match(wm_class='confirm'),
    Match(wm_class='dialog'),
    Match(wm_class='download'),
    Match(wm_class='error'),
    Match(wm_class='file_progress'),
    Match(wm_class='notification'),
    Match(wm_class='splash'),
    Match(wm_class='toolbar'),
    Match(wm_class='confirmreset'),
    Match(wm_class='makebranch'),
    Match(wm_class='maketag'),
    Match(wm_class='Arandr'),
    Match(wm_class='feh'),
    Match(wm_class='Galculator'),
    Match(wm_class='arcolinux-logout'),
    Match(wm_class='xfce4-terminal'),
    Match(wm_class='branchdialog'),
    Match(wm_class='Open File'),
    Match(wm_class='Open Folder'),
    Match(wm_class='pinentry'),
    Match(wm_class='Reminders'),
    Match(wm_class='Open Project'),
    Match(wm_class='Confirm Exit'),
    Match(wm_class='ssh-askpass'),
],
border_width=2, 
border_focus="#3384d0", 
border_normal="#4c566a", 
fullscreen_border_width = 0)
auto_fullscreen = True

focus_on_window_activation = "smart" # or focus  

wmname = "LG3D"
