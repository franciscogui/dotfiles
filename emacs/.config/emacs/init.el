;; LOAD FILE WHERE EMACS AUTOMATICALLY GENERATED CONFIGURATIONS ARE STORED
(setq custom-file "~/.config/emacs/.emacs-custom")
(load custom-file 'noerror)

;; LINE NUMBERS COLUMN CONFIGURATIONS
(global-display-line-numbers-mode t)
(menu-bar-display-line-numbers-mode 'relative)
(column-number-mode)

;; SETUP PACKAGE REPOSITORIES AND USE-PACKAGE
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
			 ("org" . "https://orgmode.org/elpa/")
		        ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(require 'use-package)
(setq use-package-always-ensure t)

;; SETUP VERTICO + MARGINALIA
(use-package vertico ;; vertical completion ui
  :ensure t
  :init
  (vertico-mode 1))

(use-package marginalia ;; provides  anotation to completion candidates
  ;; Either bind `marginalia-cycle' globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
	 :map minibuffer-local-map
	 ("M-A" . marginalia-cycle))
  ;; The :init configuration is always executed (Not lazy!)
  :init
  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))

;; CONSULT
(use-package consult
    :ensure t
    :hook (completion-list-mode . consult-preview-at-point-mode)
    :bind
    (:map global-map
      ("M-g M-g" . consult-goto-line)
;;      ("M-K" . consult-keep-lines) ; M-S-k is similar to M-S-5 (M-%)
;;      ("M-F" . consult-focus-lines) ; same principle
      ("M-s M-b" . consult-buffer)
;;      ("M-s M-f" . consult-find)
;;    ("M-s M-g" . consult-grep)
;;      ("M-s M-h" . consult-history)
      ("M-s M-i" . consult-imenu)
      ("M-s M-l" . consult-line)
      ("M-s M-m" . consult-mark)
      ("M-s M-y" . consult-yank-pop)
      ("M-s M-s" . consult-outline)
;;     :map consult-narrow-map
;;      ("?" . consult-narrow-help))
    :config
    (setq consult-line-numbers-widen t)
    ;; (setq completion-in-region-function #'consult-completion-in-region)
    (setq consult-async-min-input 3)
    (setq consult-async-input-debounce 0.5)
    (setq consult-async-input-throttle 0.8)
    (setq consult-narrow-key nil)
    (setq consult-find-args
          (concat "find . -not ( "
                  "-path */.git* -prune "
                  "-or -path */.cache* -prune )"))
    (setq consult-preview-key 'any)
    (setq consult-project-function nil) ; always work from the current directory (use `cd' to switch directory)

    (add-to-list 'consult-mode-histories '(vc-git-log-edit-mode . log-edit-comment-ring))

 ;;   (require 'consult-imenu) ; the `imenu' extension is in its own file
)




    
;; SETUP BEACON 
(use-package beacon ;; highlights your cursor when you scroll
  :config
  (beacon-mode 1)
  (setq beacon-blink-duration 0.6
	beacon-\blink-delay 0.4
	beacon-color "#0bdfff"))


