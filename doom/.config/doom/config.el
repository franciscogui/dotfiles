;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Francisco Guimaraes"
      User-mail-address "francisco@franciscogui.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-ir-black)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' :when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; magit section
(after! magit (setq magit-repository-directories '(("~/Work/thula/sfm/repos/sfm-1/SFM" . 2))))


;; org section
(after! org
  (setq org-directory "~/nextcloud/org")
  ;; If you use `org' and don't want your org files in the default location below,
  ;; change `org-directory'. It must be set before org loads!
  (setq org-log-done 'time)
  (setq org-blank-before-new-entry '((heading . t) (plain-list-item . t)))
  (setq org-capture-templates
               '(("t" "Todo" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "TASKS")
                "* TODO %i%?")
                ("S" "Tickler S" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "TICKLER")
                "* TODO %i%? \nSCHEDULED: %^t")
                ("D" "Tickler D" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "TICKLER")
                "* TODO %i%? \nDEADLINE: %^t")
                ("n" "Note" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "NOTES")
                "* %i%?")
                ("i" "Idea" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "IDEAS")
                "* IDEA %i%?")
                ("Q" "Quote" entry
                (file+headline "~/nextcloud/org/gtd/inbox.org" "QUOTES")
                "* QUOTE %i%?")))
 (setq org-agenda-files '("~/nextcloud/org/gtd/inbox.org" "~/nextcloud/org/gtd/projects.org" "~/nextcloud/org/gtd/tickler.org" "~/nextcloud/org/gtd/next.org"))
 (setq org-tag-alist '(("@home" . ?h) ("@thula" . ?w) ("@thulapt" . ?t) ("@workpersonal" . ?p) ("@reading" . ?r) ("@technical" . ?z) (:newline) ))
)

(use-package! org-superstar
  :after evil-org
  :config
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

;; custom functions section

;; pretty xml
;; https://gist.github.com/DinoChiesa/5489021
(defun dino-xml-pretty-print-region (begin end)
  "Pretty format XML markup in region. You need to have nxml-mode
    http://www.emacswiki.org/cgi-bin/wiki/NxmlMode installed to do
    this. The function inserts linebreaks to separate tags that have
    nothing but whitespace between them. It then indents the markup
    by using nxml's indentation rules."
  (interactive "r")
  (save-excursion
    (nxml-mode)
    ;; split <foo><bar> or </foo><bar>, but not <foo></foo>
    (goto-char begin)
    (while (search-forward-regexp ">[ \t]*<[^/]" end t)
      (backward-char 2) (insert "\n") (incf end))
    ;; split <foo/></foo> and </foo></foo>
    (goto-char begin)
    (while (search-forward-regexp "<.*?/.*?>[ \t]*<" end t)
      (backward-char) (insert "\n") (incf end))
    ;; put xml namespace decls on newline
    (goto-char begin)
    (while (search-forward-regexp "\\(<\\([a-zA-Z][-:A-Za-z0-9]*\\)\\|['\"]\\) \\(xmlns[=:]\\)" end t)
      (goto-char (match-end 0))
      (backward-char 6) (insert "\n") (incf end))
    (indent-region begin end nil)
    (normal-mode))
  (message "All indented!"))


(defun dino-xml-pretty-print-buffer ()
  "pretty print the XML in a buffer."
  (interactive)
  (dino-xml-pretty-print-region (point-min) (point-max)))


(global-set-key (kbd "<left>")
  (lambda()
    (interactive)
    (message "Use Vim keys: h for Left you bastard you")))

(global-set-key (kbd "<right>")
  (lambda()
    (interactive)
    (message "Use Vim keys: l for Right you bastard you")))

(global-set-key (kbd "<up>")
  (lambda()
    (interactive)
    (message "Use Vim keys: k for Up you bastard you")))

(global-set-key (kbd "<down>")
  (lambda()
    (interactive)
    (message "Use Vim keys: j for Down you bastard you")))

(map! :leader
      (:prefix ("r" . "neotree")
       :desc "open"
       "o" #'neotree
       :desc "hide"
       "h" #'neotree-hide))


(after! denote

;; Remember to check the doc strings of those variables.
(setq denote-directory (expand-file-name "~/nextcloud/org/notes/"))
(setq denote-known-keywords '("emacs" "philosophy" "tech" "life"))
(setq denote-infer-keywords t)
(setq denote-allow-multi-word-keywords t)
(setq denote-sort-keywords t)
(setq denote-file-type nil) ; Org is the default, set others here
(setq denote-prompts '(title keywords)))

;; Pick dates, where relevant, with Org's advanced interface:
(setq denote-date-prompt-use-org-read-date t)

(setq-hook! '+doom-dashboard-mode-hook default-directory "~")
;; dired configurations

(setq delete-by-moving-to-trash t
      trash-directory "~/.local/share/Trash/files/")
