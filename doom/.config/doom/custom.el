(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#323f4e" "#f48fb1" "#53e2ae" "#f1fa8c" "#92b6f4" "#BD99FF" "#79e6f3" "#f8f8f2"])
 '(custom-safe-themes
   '("da186cce19b5aed3f6a2316845583dbee76aea9255ea0da857d1c058ff003546" "e6f3a4a582ffb5de0471c9b640a5f0212ccf258a987ba421ae2659f1eaa39b09" "850bb46cc41d8a28669f78b98db04a46053eca663db71a001b40288a9b36796c" "4b6b6b0a44a40f3586f0f641c25340718c7c626cbf163a78b5a399fbe0226659" "b5803dfb0e4b6b71f309606587dd88651efe0972a5be16ece6a958b197caeed8" "266ecb1511fa3513ed7992e6cd461756a895dcc5fef2d378f165fed1c894a78c" "333958c446e920f5c350c4b4016908c130c3b46d590af91e1e7e2a0611f1e8c5" "5f19cb23200e0ac301d42b880641128833067d341d22344806cdad48e6ec62f6" "0466adb5554ea3055d0353d363832446cd8be7b799c39839f387abb631ea0995" "84b14a0a41bb2728568d40c545280dbe7d6891221e7fbe7c2b1c54a3f5959289" "23c806e34594a583ea5bbf5adf9a964afe4f28b4467d28777bcba0d35aa0872e" "c4063322b5011829f7fdd7509979b5823e8eea2abf1fe5572ec4b7af1dd78519" "a9a67b318b7417adbedaab02f05fa679973e9718d9d26075c6235b1f0db703c8" "1d44ec8ec6ec6e6be32f2f73edf398620bb721afeed50f75df6b12ccff0fbb15" "6c98bc9f39e8f8fd6da5b9c74a624cbb3782b4be8abae8fd84cbc43053d7c175" "e2c926ced58e48afc87f4415af9b7f7b58e62ec792659fcb626e8cba674d2065" "846b3dc12d774794861d81d7d2dcdb9645f82423565bfb4dad01204fa322dbd5" "6c531d6c3dbc344045af7829a3a20a09929e6c41d7a7278963f7d3215139f6a7" "c2aeb1bd4aa80f1e4f95746bda040aafb78b1808de07d340007ba898efa484f5" "1704976a1797342a1b4ea7a75bdbb3be1569f4619134341bd5a4c1cfb16abad4" "7a7b1d475b42c1a0b61f3b1d1225dd249ffa1abb1b7f726aec59ac7ca3bf4dae" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" "745d03d647c4b118f671c49214420639cb3af7152e81f132478ed1c649d4597d" "a82ab9f1308b4e10684815b08c9cac6b07d5ccb12491f44a942d845b406b0296" "e8df30cd7fb42e56a4efc585540a2e63b0c6eeb9f4dc053373e05d774332fc13" "f7fed1aadf1967523c120c4c82ea48442a51ac65074ba544a5aefc5af490893b" "f6665ce2f7f56c5ed5d91ed5e7f6acb66ce44d0ef4acfaa3a42c7cfe9e9a9013" "234dbb732ef054b109a9e5ee5b499632c63cc24f7c2383a849815dacc1727cb6" "8146edab0de2007a99a2361041015331af706e7907de9d6a330a3493a541e5a6" "1d5e33500bc9548f800f9e248b57d1b2a9ecde79cb40c0b1398dec51ee820daf" default))
 '(exwm-floating-border-color "#28323e")
 '(fci-rule-color "#364455")
 '(highlight-tail-colors
   ((("#354f57" "#081611" "green")
     . 0)
    (("#394f5e" "#0d1617" "brightcyan")
     . 20)))
 '(jdee-db-active-breakpoint-face-colors (cons "#181e26" "#87DFEB"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#181e26" "#53e2ae"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#181e26" "#3d4c5f"))
 '(objed-cursor-color "#f48fb1")
 '(pdf-view-midnight-colors (cons "#f8f8f2" "#323f4e"))
 '(rustic-ansi-faces
   ["#323f4e" "#f48fb1" "#53e2ae" "#f1fa8c" "#92b6f4" "#BD99FF" "#79e6f3" "#f8f8f2"])
 '(vc-annotate-background "#323f4e")
 '(vc-annotate-color-map
   (list
    (cons 20 "#53e2ae")
    (cons 40 "#87eaa2")
    (cons 60 "#bcf297")
    (cons 80 "#f1fa8c")
    (cons 100 "#f1dc83")
    (cons 120 "#f1bf7a")
    (cons 140 "#f2a272")
    (cons 160 "#e09fa1")
    (cons 180 "#ce9cd0")
    (cons 200 "#BD99FF")
    (cons 220 "#cf95e5")
    (cons 240 "#e192cb")
    (cons 260 "#f48fb1")
    (cons 280 "#c67e9c")
    (cons 300 "#986d88")
    (cons 320 "#6a5c73")
    (cons 340 "#364455")
    (cons 360 "#364455")))
 '(vc-annotate-very-old-color nil))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
